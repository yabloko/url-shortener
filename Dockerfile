# Please note that this image is for demo mode only,
# in production env you dont want to run all processes in one container, k8s FTW

FROM php:7.1-fpm

COPY . /var/www/apps/url-shortener

#skip vendors and cache dirs, if built from development workspace
RUN rm -rf /var/www/apps/url-shortener/vendor && rm -rf /var/www/apps/url-shortener/var

WORKDIR /var/www/apps/url-shortener


RUN apt-get update -q && \
    apt-get install --yes -q --no-install-recommends \
	nginx \
	git \
	wget \
	zip \
	unzip \
    redis-server

RUN pecl install redis && echo "extension=redis.so" >> /usr/local/etc/php/php.ini
RUN echo "clear_env = no" >> /usr/local/etc/php-fpm.conf

RUN curl -sSL https://getcomposer.org/composer.phar -o /usr/bin/composer && \
    chmod +x /usr/bin/composer
RUN composer install

RUN rm /etc/nginx/sites-enabled/default
COPY config/nginx/url-shortener.conf /etc/nginx/sites-enabled/
RUN mkdir /var/www/apps/url-shortener/var && chown -R www-data /var/www/apps/url-shortener/var

EXPOSE 80

ENV REDIS_HOST=127.0.0.1
ENV REDIS_STORAGE_PORT=6379
ENV REDIS_SEQUENCE_PORT=6380

RUN echo "#/bin/sh \n\
echo \"port $REDIS_STORAGE_PORT\" | redis-server - &\n\
echo \"port $REDIS_SEQUENCE_PORT\" | redis-server - &" >> run_redis.sh && chmod 755 run_redis.sh

CMD ./run_redis.sh && /usr/sbin/nginx && php-fpm

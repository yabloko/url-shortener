<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Tests\UrlShortener\Infrastructure\Domain\ShortenedUrl;

use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlRepositoryInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\SourceUrl;
use Yabloko\UrlShortener\Infrastructure\Domain\ShortenedUrl\RedisShortenedUrlRepository;

class RedisShortenedUrlRepositoryTest extends TestCase
{
    /** @var \Redis|MockInterface*/
    private $redis;

    /** @var ShortenedUrlRepositoryInterface */
    private $repository;

    public function setUp()
    {
        $this->redis = \Mockery::mock(\Redis::class);
        $this->repository = new RedisShortenedUrlRepository($this->redis);
    }

    /**
     * @test
     */
    public function itShouldFindRecordIfExists()
    {
        $expectedShortUrl = 'abc';
        $shortenedUrlId = ShortenedUrlId::create();
        $expectedSourceUrl = 'http://example.com';

        $this->redis->shouldReceive('get')->andReturnUsing(function ($key) use ($shortenedUrlId, $expectedShortUrl, $expectedSourceUrl) {
            if ($key === (string) $shortenedUrlId) {
                return sprintf('{"shortUrl":"%s","sourceUrl":"%s","id":"%s"}', $expectedShortUrl, $expectedSourceUrl, $shortenedUrlId);
            }

            return false;
        });

        $shortenedUrl = $this->repository->find($shortenedUrlId);

        $this->assertTrue(ShortUrl::fromValue($expectedShortUrl)->equals($shortenedUrl->getShortUrl()));
        $this->assertTrue(SourceUrl::fromValue($expectedSourceUrl)->equals($shortenedUrl->getSourceUrl()));
        $this->assertTrue($shortenedUrlId->equals($shortenedUrl->getId()));
    }

    /**
     * @test
     */
    public function itShouldFindRecordBySourceUrlIfExists()
    {
        $expectedShortUrl = 'abc';
        $shortenedUrlId = ShortenedUrlId::create();
        $expectedSourceUrl = 'http://example.com';

        $this->redis->shouldReceive('get')->andReturnUsing(function ($key) use ($shortenedUrlId, $expectedShortUrl, $expectedSourceUrl) {
            if ($key === (string) $shortenedUrlId) {
                return sprintf('{"shortUrl":"%s","sourceUrl":"%s","id":"%s"}', $expectedShortUrl, $expectedSourceUrl, $shortenedUrlId);
            }

            return false;
        });
        $this->redis->shouldReceive('keys')->andReturn([(string) $shortenedUrlId]);

        $shortenedUrl = $this->repository->findBySourceUrl(SourceUrl::fromValue($expectedSourceUrl));

        $this->assertTrue(ShortUrl::fromValue($expectedShortUrl)->equals($shortenedUrl->getShortUrl()));
        $this->assertTrue(SourceUrl::fromValue($expectedSourceUrl)->equals($shortenedUrl->getSourceUrl()));
        $this->assertTrue($shortenedUrlId->equals($shortenedUrl->getId()));
    }

    /**
     * @test
     */
    public function itShouldFindRecordByShortUrlIfExists()
    {
        $expectedShortUrl = 'abc';
        $shortenedUrlId = ShortenedUrlId::create();
        $expectedSourceUrl = 'http://example.com';

        $this->redis->shouldReceive('get')->andReturnUsing(function ($key) use ($shortenedUrlId, $expectedShortUrl, $expectedSourceUrl) {
            if ($key === (string) $shortenedUrlId) {
                return sprintf('{"shortUrl":"%s","sourceUrl":"%s","id":"%s"}', $expectedShortUrl, $expectedSourceUrl, $shortenedUrlId);
            }

            return false;
        });
        $this->redis->shouldReceive('keys')->andReturn([(string) $shortenedUrlId]);

        $shortenedUrl = $this->repository->findByShortUrl(ShortUrl::fromValue($expectedShortUrl));

        $this->assertTrue(ShortUrl::fromValue($expectedShortUrl)->equals($shortenedUrl->getShortUrl()));
        $this->assertTrue(SourceUrl::fromValue($expectedSourceUrl)->equals($shortenedUrl->getSourceUrl()));
        $this->assertTrue($shortenedUrlId->equals($shortenedUrl->getId()));
    }

    /**
     * @test
     */
    public function itShouldReturnNullIfRecordDoesntExist()
    {
        $shortenedUrlId = ShortenedUrlId::create();

        $this->redis->shouldReceive('get')->andReturn(false);

        $shortenedUrl = $this->repository->find($shortenedUrlId);

        $this->assertNull($shortenedUrl);
    }

    /**
     * @test
     */
    public function itShouldGetAllRecords()
    {
        $expectedShortUrl = 'abc';
        $shortenedUrlId = ShortenedUrlId::create();
        $expectedSourceUrl = 'http://example.com';
        $expectedRecordCount = 1;

        $this->redis->shouldReceive('get')->andReturnUsing(function ($key) use ($shortenedUrlId, $expectedShortUrl, $expectedSourceUrl) {
            if ($key === (string) $shortenedUrlId) {
                return sprintf('{"shortUrl":"%s","sourceUrl":"%s","id":"%s"}', $expectedShortUrl, $expectedSourceUrl, $shortenedUrlId);
            }

            return false;
        });
        $this->redis->shouldReceive('keys')->andReturn([(string) $shortenedUrlId]);

        $shortenedUrls = $this->repository->getAll();

        $this->assertCount($expectedRecordCount, $shortenedUrls);
        $shortenedUrl = $shortenedUrls[0];

        $this->assertTrue(ShortUrl::fromValue($expectedShortUrl)->equals($shortenedUrl->getShortUrl()));
        $this->assertTrue(SourceUrl::fromValue($expectedSourceUrl)->equals($shortenedUrl->getSourceUrl()));
        $this->assertTrue($shortenedUrlId->equals($shortenedUrl->getId()));
    }

    /**
     * @test
     */
    public function itShouldSaveRecord()
    {
        $expectedShortUrl = ShortUrl::fromValue('abc');
        $shortenedUrlId = ShortenedUrlId::create();
        $expectedSourceUrl = SourceUrl::fromValue('http://example.com');
        $shortenedUrl = ShortenedUrl::fromValues($shortenedUrlId, $expectedShortUrl, $expectedSourceUrl);

        $this->redis->shouldReceive('exists')->andReturn(false);

        $str = json_encode(['id' => (string) $shortenedUrlId,'shortUrl' => (string) $expectedShortUrl, 'sourceUrl' => (string) $expectedSourceUrl]);
        $this->redis->shouldReceive('append')->withArgs([(string) $shortenedUrlId, $str]);

        $this->repository->save($shortenedUrl);
    }

    /**
     * @test
     * @expectedException \RuntimeException
     */
    public function itShouldThrowIfAlreadyExistsOnSaveRecord()
    {
        $expectedShortUrl = ShortUrl::fromValue('abc');
        $shortenedUrlId = ShortenedUrlId::create();
        $expectedSourceUrl = SourceUrl::fromValue('http://example.com');
        $shortenedUrl = ShortenedUrl::fromValues($shortenedUrlId, $expectedShortUrl, $expectedSourceUrl);

        $this->redis->shouldReceive('exists')->andReturnUsing(function ($key) use ($shortenedUrl) {
            if ($key === (string) $shortenedUrl->getId()) {
                return true;
            }

            return false;
        });

        $this->repository->save($shortenedUrl);
    }

    /**
     * @test
     * @expectedException \RuntimeException
     */
    public function itShouldThrowIfStorageIsCorrupted()
    {
        $shortenedUrlId = ShortenedUrlId::create();

        $this->redis->shouldReceive('get')->andReturn('{"malformed":"json');

        $this->repository->find($shortenedUrlId);
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Tests\UrlShortener\Domain\ShortenedUrl;

use PHPUnit\Framework\TestCase;
use Yabloko\UrlShortener\Domain\ShortenedUrl\SourceUrl;

class SourceUrlTest extends TestCase
{
    /**
     * @test
     * @dataProvider validSourceUrlProvider
     */
    public function itShouldCreateFromValidValue(string $sourceValue)
    {
        $sourceUrl = SourceUrl::fromValue($sourceValue);

        $this->assertEquals($sourceValue, (string) $sourceUrl);
    }

    /**
     * @test
     * @dataProvider invalidSourceUrlProvider
     * @expectedException \InvalidArgumentException
     */
    public function itShouldThrowIfCreatedFromInvalidValue(string $sourceValue)
    {
        SourceUrl::fromValue($sourceValue);
    }

    /**
     * @test
     */
    public function itShouldBeEqualToSameSourceUrl()
    {
        $sourceUrlValue = 'http://example.com';
        $sourceUrl = SourceUrl::fromValue($sourceUrlValue);
        $sameSourceUrl = SourceUrl::fromValue($sourceUrlValue);

        $this->assertTrue($sourceUrl->equals($sameSourceUrl));
    }

    /**
     * @test
     */
    public function itShouldNotBeEqualToOtherSourceUrl()
    {
        $sourceUrlValue = 'http://example.com';
        $otherSourceUrlValue = 'https://example.com';
        $sourceUrl = SourceUrl::fromValue($sourceUrlValue);
        $otherSourceUrl = SourceUrl::fromValue($otherSourceUrlValue);

        $this->assertFalse($sourceUrl->equals($otherSourceUrl));
    }

    public static function validSourceUrlProvider()
    {
        return [
            'http' => ['sourceValue' => 'http://example.com'],
            'https' => ['sourceValue' => 'https://example.com'],
            'with query string' => ['sourceValue' => 'http://example.com?key=value'],
        ];
    }

    public static function invalidSourceUrlProvider()
    {
        return [
            'ftp' => ['sourceValue' => 'ftp://example.com'],
            'non-existing protocol' => ['sourceValue' => 'httpc://example.com'],
            'not allowed characters in host' => ['sourceValue' => 'http://exa%mple.com'],
        ];
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Tests\UrlShortener\Domain\ShortenedUrl;

use PHPUnit\Framework\TestCase;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortUrl;

class ShortUrlTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldCreateFromValidValues()
    {
        $shortValue = 'abcd';
        $shortUrl = ShortUrl::fromValue($shortValue);

        $this->assertEquals($shortValue, (string) $shortUrl);
    }

    /**
     * @test
     * @dataProvider invalidShortUrlProvider
     * @expectedException \InvalidArgumentException
     */
    public function itShouldThrowIfCreatedFromInvalidValues(string $shortValue)
    {
        ShortUrl::fromValue($shortValue);
    }

    /**
     * @test
     */
    public function itShouldBeEqualToSameShortUrl()
    {
        $shortValue = 'abcd';
        $shortUrl = ShortUrl::fromValue($shortValue);
        $otherShortUrl = ShortUrl::fromValue($shortValue);

        $this->assertTrue($shortUrl->equals($otherShortUrl));
    }

    /**
     * @test
     */
    public function itShouldNotBeEqualToOtherShortUrl()
    {
        $shortValue = 'abcd';
        $otherShortValue = 'dcba';
        $shortUrl = ShortUrl::fromValue($shortValue);
        $otherShortUrl = ShortUrl::fromValue($otherShortValue);

        $this->assertFalse($shortUrl->equals($otherShortUrl));
    }

    public static function invalidShortUrlProvider()
    {
        return [
            'too long' => ['qwertyuio'],
            'not allowed characters' => ['qw$qwe'],
        ];
    }
}

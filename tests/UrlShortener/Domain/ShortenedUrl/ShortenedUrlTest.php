<?php

declare(strict_types = 1);

namespace Yabloko\UrlShortener\Tests\UrlShortener\Domain\ShortenedUrl;


use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Rhumsaa\Uuid\Uuid;
use Yabloko\UrlShortener\Domain\SequenceGenerator\SequenceGeneratorInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\SourceUrl;

class ShortenedUrlTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldCreateFromValues()
    {
        $shortUrl = ShortUrl::fromValue('a');
        $sourceUrl = SourceUrl::fromValue('http://example.com');
        $expectedSignature = (string) Uuid::uuid4();
        $id = ShortenedUrlId::fromSignature($expectedSignature);

        $shortenedUrl = ShortenedUrl::fromValues($id, $shortUrl, $sourceUrl);

        $this->assertTrue($sourceUrl->equals($shortenedUrl->getSourceUrl()));
        $this->assertTrue($shortUrl->equals($shortenedUrl->getShortUrl()));
        $this->assertTrue($id->equals($shortenedUrl->getId()));
    }

    /**
     * @test
     */
    public function itShouldCreateFromSourceUrlWithSequenceGenerator()
    {
        $sourceUrl = SourceUrl::fromValue('http://example.com');
        $expectedShortUrl = ShortUrl::fromValue('r');
        $expectedSignature = (string) Uuid::uuid4();
        $id = ShortenedUrlId::fromSignature($expectedSignature);

        /** @var SequenceGeneratorInterface|ObjectProphecy $sequenceGenerator */
        $sequenceGenerator = $this->prophesize(SequenceGeneratorInterface::class);
        $sequenceGenerator->next()->willReturn(1);

        $shortenedUrl = ShortenedUrl::createWithSequenceGenerator($id, $sourceUrl, $sequenceGenerator->reveal());

        $this->assertTrue($sourceUrl->equals($shortenedUrl->getSourceUrl()));
        $this->assertTrue($expectedShortUrl->equals($shortenedUrl->getShortUrl()));
        $this->assertTrue($id->equals($shortenedUrl->getId()));
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Tests\UrlShortener\Domain\ShortenedUrl;

use Assert\Assert;
use PHPUnit\Framework\TestCase;
use Rhumsaa\Uuid\Uuid;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;

class ShortenedUrlIdTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldCreateFromSignature()
    {
        $signature = (string) Uuid::uuid4();

        $shortenedUrlId = ShortenedUrlId::fromSignature($signature);

        $this->assertEquals($signature, (string) $shortenedUrlId);
    }

    /**
     * @test
     */
    public function itShouldCreate()
    {
        $shortenedUrlId = ShortenedUrlId::create();

        $this->assertNotEmpty($shortenedUrlId);
        Assert::that((string) $shortenedUrlId)->uuid();
    }

    /**
     * @test
     */
    public function itShouldBeEqualToSameId()
    {
        $signature = (string) Uuid::uuid4();

        $shortenedUrlId = ShortenedUrlId::fromSignature($signature);
        $sameShortenedUrlId = ShortenedUrlId::fromSignature($signature);

        $this->assertTrue($shortenedUrlId->equals($sameShortenedUrlId));
    }

    /**
     * @test
     */
    public function itShouldNotBeEqualToOtherId()
    {
        $signature = (string) Uuid::uuid4();
        $otherSignature = (string) Uuid::uuid4();

        $shortenedUrlId = ShortenedUrlId::fromSignature($signature);
        $otherShortenedUrlId = ShortenedUrlId::fromSignature($otherSignature);

        $this->assertFalse($shortenedUrlId->equals($otherShortenedUrlId));
    }
}

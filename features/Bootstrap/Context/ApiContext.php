<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Features\Context;

use Assert\Assert;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Tester\Exception\PendingException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yabloko\UrlShortener\Features\HttpApi\KernelClient;

class ApiContext implements Context
{
    /** @var  KernelClient */
    private $client;

    /** @var Response */
    private $response;

    public function __construct()
    {
        $this->client = KernelClient::getInstance();
    }

    /**
     * @When /^I do POST \'([^\']*)\' to \'([^\']*)\'$/
     */
    public function iDoPOSTKeyWithValueTo($postPayload, $uri)
    {
        $this->response = $this->client->request($uri, Request::METHOD_POST, [], [], $postPayload);
    }

    /**
     * @Then I should see HTTP response with status :statusCode
     */
    public function iShouldSeeHTTPResponseWithStatus(int $statusCode)
    {
        Assert::that($statusCode)->eq($this->response->getStatusCode());
    }

    /**
     * @Then I should see HTTP response with header :header equal to :value
     */
    public function iShouldSeeHTTPResponseWithHeaderEqualValue(string $header, string $value)
    {
        $actualtHeaderValue = $this->response->headers->get($header, null);
        Assert::that($actualtHeaderValue)
            ->notNull(sprintf('Header %s not expected in response, but not found', $header))
            ->eq($value, sprintf('Header %s expected to have %s value, but actual is %s.', $header, $value, $actualtHeaderValue));
    }

    /**
     * @Given I should see HTTP response with body :responseBody
     */
    public function iShouldSeeHTTPResponseWithBody($responseBody)
    {
        Assert::that($responseBody)->eq($this->response->getContent());
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenario(BeforeScenarioScope $scenarioEvent)
    {
        $this->response = null;
    }

    /**
     * @When /^I GET \'([^\']*)\'$/
     */
    public function iGET($uri)
    {
        $this->response = $this->client->request($uri, Request::METHOD_GET);
    }
}

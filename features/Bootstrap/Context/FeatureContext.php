<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Features\Context;

use Assert\Assert;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Yabloko\UrlShortener\Application\Query\ViewModel\ShortenedUrlViewModel;
use Yabloko\UrlShortener\Application\Query\GetByShortUrl\ShortUrlHandler;
use Yabloko\UrlShortener\Application\Query\GetByShortUrl\ShortUrlQuery;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlReadRepositoryInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortUrl;
use Yabloko\UrlShortener\Application\Command\ShortenUrl\ShortenerHandler;
use Yabloko\UrlShortener\Application\Command\ShortenUrl\ShortenUrlCommand;

class FeatureContext implements Context
{
    /** @var ShortenerHandler */
    private $shortenerUrlHandler;

    /** @var array */
    private $exceptions;

    /** @var ShortenedUrlReadRepositoryInterface */
    private $shortenedUrlRepository;

    /** @var ShortenedUrlId */
    private $createdRecordId;

    /** @var \Redis */
    private $redisStorage;

    /** @var \Redis */
    private $redisSequence;

    /** @var ShortUrlHandler */
    private $shortUrlQueryHandler;

    /** @var ShortenedUrlViewModel */
    private $shortenedUrlViewModel;

    public function __construct()
    {
        /** @var ContainerInterface $container */
        $container = $_ENV['container'];
        $this->shortenerUrlHandler = $container->get('test.url_shortener.application.command_handler.shortener_handler');
        $this->shortUrlQueryHandler = $container->get('test.url_shortener.application.query_handler.short_url_handler');
        $this->shortenedUrlRepository = $container->get('test.url_shortener.infrastructure.redis_shortened_url_repository');
        $this->redisStorage = $container->get('test.url_shortener.redis_storage.client');
        $this->redisSequence = $container->get('test.url_shortener.redis_sequence.client');
    }


    /**
     * @BeforeScenario
     */
    public function beforeScenario(BeforeScenarioScope $scenarioEvent)
    {
        $this->cleanUp();
        $this->cleanUpStorage();
        $this->resetCollectedExceptions();
    }

    /**
     * @When /^I pass URL \'([^\']*)\' to shortener$/
     */
    public function iPassURLToShortener(string $url)
    {
        $id = (string) Uuid::uuid4();
        $shortenCommand = new ShortenUrlCommand($url, $id);
        try {
            $this->createdRecordId = $this->shortenerUrlHandler->handle($shortenCommand);
        } catch (\Exception $e) {
            $this->exceptions[] = $e;
        }
    }

    /**
     * @Then /^I should see \'([^\']*)\' as shortened URL$/
     */
    public function iShouldSeeAsShortenedURL($shortUrlValue)
    {
        $shortenedUrl = $this->shortenedUrlRepository->find($this->createdRecordId);
        Assert::that($shortenedUrl->getShortUrl()->equals(ShortUrl::fromValue($shortUrlValue)))->true(sprintf('Expected value is %s but %s given', $shortUrlValue, $shortenedUrl->getShortUrl()));
    }

    /**
     * @Then /^I should see error message$/
     */
    public function iShouldSeeErrorMessage()
    {
        Assert::that($this->exceptions)->count(1);
        Assert::that($this->exceptions[0])->isInstanceOf(\Exception::class);
        $this->exceptions = [];
    }

    /**
     * @When /^I request short URL \'([^\']*)\' from shortener$/
     */
    public function iRequestShortURLFromShortener($shortUrl)
    {
        $this->shortenedUrlViewModel = $this->shortUrlQueryHandler->handle(new ShortUrlQuery($shortUrl));
    }

    /**
     * @Then /^I should see original URL \'([^\']*)\'$/
     */
    public function iShouldSeeOriginalURL($sourceUrl)
    {
        $actualSourceUrl = $this->shortenedUrlViewModel->getSourceUrl();
        Assert::that($sourceUrl)->eq($actualSourceUrl, sprintf('Source URL expected to be %s, but %s found.', $sourceUrl, $actualSourceUrl));
    }

    /**
     * @Then /^I should see empty value$/
     */
    public function iShouldSeeEmptyValue()
    {
        Assert::that($this->shortenedUrlViewModel)->null('Source URL expected to be null.');
    }

    private function cleanUp()
    {
        $this->createdRecordId = null;
        $this->shortenedUrlViewModel = null;
    }

    private function resetCollectedExceptions()
    {
        Assert::that($this->exceptions)->count(0, sprintf('No unchecked exceptions are expected after this scenario, but %d left.', count($this->exceptions)));
    }

    private function cleanUpStorage()
    {
        $this->redisStorage->flushDB();
        $this->redisSequence->flushDB();
    }
}

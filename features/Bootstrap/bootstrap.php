<?php

declare(strict_types=1);

use Symfony\Component\Debug\Debug;

require __DIR__ . '/../../app/autoload.php';

Debug::enable();

$container = \Yabloko\UrlShortener\Features\HttpApi\KernelClient::getInstance()->getContainer();

$_ENV['container'] = $container;

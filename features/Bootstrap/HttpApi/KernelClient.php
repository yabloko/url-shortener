<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Features\HttpApi;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class KernelClient
{
    /**
     * @var \AppKernel */
    private static $kernel;

    /** @var KernelClient */
    private static $instance;

    private function __construct()
    {
        self::$kernel = new \AppKernel('test', true);
        self::$kernel->boot();
    }

    public static function getInstance(): self
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    public function request(string $uri, string $method = Request::METHOD_GET, array $parameters = [], array $server = [], string $content = null): Response
    {
        $request = Request::create($uri, $method, $parameters, [], [], $server, $content);

        /*
         * because \Symfony\Component\HttpFoundation\Response::sendContent
         * has
         * echo $this->content;
         * which is cluttering CLI script output
         */
        ob_start();
        $response = self::$kernel->handle($request)->send();
        ob_end_clean();

        return $response;
    }

    public function getContainer(): ContainerInterface
    {
        return self::$kernel->getContainer();
    }
}

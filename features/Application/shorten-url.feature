Feature: As a user, I want to shorten a URL, so I can use this shorten URL in text messages w/o cluttering the message

  Scenario: URL is valid and its shortened
    When I pass URL 'http://example.com' to shortener
    Then I should see 'r' as shortened URL

  Scenario: URL is invalid and its shortened
    When I pass URL 'not-a-valid-url' to shortener
    Then I should see error message

   Scenario: if valid URL was shortened already, I should see the same value
     Given I pass URL 'http://example.com' to shortener
     And I should see 'r' as shortened URL
     When I pass URL 'http://example.com' to shortener
     Then I should see 'r' as shortened URL

Feature: As a user, I want to access the original URL when using the shorten one, so that I can use both URLs interchangeably

  Scenario: valid URL was shortened before, and I request it
    Given I pass URL 'http://example.com' to shortener
    When I request short URL 'r' from shortener
    Then I should see original URL 'http://example.com'

  Scenario: valid URL was not shortened before, and I request it
    When I request short URL 'r' from shortener
    Then I should see empty value

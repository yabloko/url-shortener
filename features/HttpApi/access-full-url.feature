Feature: As a user, I want to access the original URL when using the shorten one, so that I can use both URLs interchangeably

  Scenario: valid URL was shortened before, and I request it
    Given I pass URL 'http://example.com' to shortener
    When I GET '/r'
    Then I should see HTTP response with status '301'
    And I should see HTTP response with header 'Location' equal to 'http://example.com'

  Scenario: valid URL was not shortened before, and I request it
    When I GET '/r'
    Then I should see HTTP response with status '404'

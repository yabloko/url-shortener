Feature: As a user, I want to shorten a URL, so I can use this shorten URL in text messages w/o cluttering the message

  Scenario: URL is valid and its shortened
    When I do POST '{"source_url":"http://example.com"}' to '/'
    Then I should see HTTP response with status '200'
    And I should see HTTP response with body '{"shortUrl":"\/r","sourceUrl":"http:\/\/example.com"}'

  Scenario: URL is invalid and its shortened
    When I do POST '{"source_url":"ftp://example.com"}' to '/'
    Then I should see HTTP response with status '400'

  Scenario: Invalid POST payload is passed - malformed json
    When I do POST '{"source_url":"http://example.com"' to '/'
    Then I should see HTTP response with status '400'

  Scenario: Invalid POST payload is passed - missing required property
    When I do POST '{"url":"http://example.com"}' to '/'
    Then I should see HTTP response with status '400'

URL shortener application
- 

Build and run with Docker:
-
 - `docker build . -t url-shortener-dev`
 - `docker run -p 127.0.0.1:%shortener_port%:80 url-shortener-dev` where %shortener_port% is any port available on your local machine
 - run tests with `docker exec -it %CONTAINER_ID% bash` and execute `bin/ci`
 
HTTP API:
 - 
 - Shorten URL:  
 
     request:
     ```
     curl -X "POST" "http://%shortener_host%/" \
                         -H 'Content-Type: application/json; charset=utf-8' \
                         -d $'{
                      "source_url": "http://example.com"
                    }'
     ```
     response:
     ```
     {"shortUrl":"\/r","sourceUrl":"http:\/\/example.com"}
     ```
 - Use shortened URL to get to full URL: 
     
     request:
     ```
     curl "http://%shortener_host%/r"
     ```
     response headers:
     ```
     HTTP/1.1 301 Moved Permanently
     Content-Type: text/html; charset=UTF-8
     Location: http://example.com
     ```

TBD:
 - 
 - HTTP REST API port with hyperlinks
 - UI (js-based?)
 - Gitlab CI pipeline

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Ports\HttpApiBundle\Controller;

use Assert\Assert;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Yabloko\UrlShortener\Application\Command\ShortenUrl\ShortenerHandler;
use Yabloko\UrlShortener\Application\Command\ShortenUrl\ShortenUrlCommand;
use Yabloko\UrlShortener\Application\Query\GetById\GetByIdHandler;
use Yabloko\UrlShortener\Application\Query\GetById\GetByIdQuery;
use Yabloko\UrlShortener\Application\Query\GetByShortUrl\ShortUrlHandler;
use Yabloko\UrlShortener\Application\Query\GetByShortUrl\ShortUrlQuery;

class UrlShortenerController
{
    private const REQUEST_PAYLOAD_KEY_NAME_SOURCE_URL = 'source_url';

    /** @var ShortenerHandler */
    private $shortenerHandler;

    /** @var GetByIdHandler */
    private $getByIdHandler;

    /** @var ShortUrlHandler */
    private $shortUrlHandler;

    public function __construct(
        ShortenerHandler $shortenerHandler,
        GetByIdHandler $getByIdHandler,
        ShortUrlHandler $shortUrlHandler
    ) {
        $this->shortenerHandler = $shortenerHandler;
        $this->getByIdHandler = $getByIdHandler;
        $this->shortUrlHandler = $shortUrlHandler;
    }

    public function shortenAction(Request $request)
    {
        try {
            $requestPayload = $this->decodeJsonOrThrow($request->getContent());
            Assert::that($requestPayload)->keyExists(self::REQUEST_PAYLOAD_KEY_NAME_SOURCE_URL, sprintf('Key %s was not found in request json payload', self::REQUEST_PAYLOAD_KEY_NAME_SOURCE_URL));
            $shortenUrlCommand = new ShortenUrlCommand($requestPayload[self::REQUEST_PAYLOAD_KEY_NAME_SOURCE_URL], (string) Uuid::uuid4());
            $shortenedUrlId = $this->shortenerHandler->handle($shortenUrlCommand);
            $shortUrl = $this->getByIdHandler->handle(new GetByIdQuery((string) $shortenedUrlId));
        } catch (\InvalidArgumentException $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(
            [
                'shortUrl' => sprintf('/%s', $shortUrl->getShortUrl()),
                'sourceUrl' => $shortUrl->getSourceUrl()
            ]
        );
    }

    public function redirectAction(string $shortUrl)
    {
        $query = new ShortUrlQuery($shortUrl);
        $shortenedUrl = $this->shortUrlHandler->handle($query);
        if(!$shortenedUrl) {
            return new Response(sprintf('%s is not a known short url', $shortUrl), Response::HTTP_NOT_FOUND);
        }

        return new RedirectResponse($shortenedUrl->getSourceUrl(), Response::HTTP_MOVED_PERMANENTLY);
    }

    private function decodeJsonOrThrow(string $jsonString): array
    {
        $decodedData = json_decode($jsonString, true);
        Assert::that(json_last_error())->eq(JSON_ERROR_NONE, sprintf('Json decode error: %s', json_last_error_msg()));

        return $decodedData;
    }
}

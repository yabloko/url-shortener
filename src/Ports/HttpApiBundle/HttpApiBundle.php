<?php

declare(strict_types = 1);

namespace Yabloko\UrlShortener\Ports\HttpApiBundle;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class HttpApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $this->loadConfigs($container);
    }

    private function loadConfigs(ContainerBuilder $container)
    {
        $rootPath = $container->getParameter('kernel.root_dir') . '/../';
        $env = $container->getParameter('kernel.environment');

        $locator = new FileLocator($rootPath);
        $loader = new YamlFileLoader($container, $locator);

        $loader->load(sprintf('src/UrlShortener/Infrastructure/DependencyInjection/config/config_%s.yml', $env));
        $loader->load('src/Ports/HttpApiBundle/Resources/config/services.yml');
    }
}

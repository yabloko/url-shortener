<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Application\Command\ShortenUrl;

final class ShortenUrlCommand
{
    /** @var string */
    public $url;

    /** @var string */
    public $id;

    public function __construct($url, $id)
    {
        $this->url = $url;
        $this->id = $id;
    }
}

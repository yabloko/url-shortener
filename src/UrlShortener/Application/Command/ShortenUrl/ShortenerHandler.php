<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Application\Command\ShortenUrl;

use Yabloko\UrlShortener\Domain\SequenceGenerator\SequenceGeneratorInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlRepositoryInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\SourceUrl;

class ShortenerHandler
{
    /** @var SequenceGeneratorInterface */
    private $sequenceGenerator;

    /** @var ShortenedUrlRepositoryInterface */
    private $shortenedUrlRepository;

    public function __construct(SequenceGeneratorInterface $sequenceGenerator, ShortenedUrlRepositoryInterface $shortenedUrlRepository)
    {
        $this->sequenceGenerator = $sequenceGenerator;
        $this->shortenedUrlRepository = $shortenedUrlRepository;
    }

    public function handle(ShortenUrlCommand $command): ShortenedUrlId
    {
        $sourceUrl = SourceUrl::fromValue($command->url);
        $id = ShortenedUrlId::fromSignature($command->id);

        if($this->shortenedUrlRepository->find($id) instanceof ShortenedUrl) {
            throw new \RuntimeException('Record with this id is already used');
        }

        $shortenedUrl = $this->shortenedUrlRepository->findBySourceUrl(SourceUrl::fromValue($command->url));

        if(!($shortenedUrl instanceof ShortenedUrl)) {
            $shortenedUrl = ShortenedUrl::createWithSequenceGenerator($id, $sourceUrl, $this->sequenceGenerator);
            $this->shortenedUrlRepository->save($shortenedUrl);
        }

        return $shortenedUrl->getId();
    }
}

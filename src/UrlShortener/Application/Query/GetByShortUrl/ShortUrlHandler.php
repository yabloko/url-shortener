<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Application\Query\GetByShortUrl;

use Yabloko\UrlShortener\Application\Query\ViewModel\ShortenedUrlViewModel;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlReadRepositoryInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortUrl;

class ShortUrlHandler
{
    /** @var ShortenedUrlReadRepositoryInterface */
    private $repository;

    public function __construct(ShortenedUrlReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(ShortUrlQuery $query): ?ShortenedUrlViewModel
    {
        $shortenedUrl = $this->repository->findByShortUrl(ShortUrl::fromValue($query->shortUrl));

        return $shortenedUrl ? ShortenedUrlViewModel::fromShortenedUrl($shortenedUrl) : null;
    }
}

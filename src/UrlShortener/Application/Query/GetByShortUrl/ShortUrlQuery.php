<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Application\Query\GetByShortUrl;

class ShortUrlQuery
{
    public $shortUrl;

    public function __construct(string $shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }
}

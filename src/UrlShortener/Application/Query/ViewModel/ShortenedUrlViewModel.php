<?php

declare(strict_types = 1);

namespace Yabloko\UrlShortener\Application\Query\ViewModel;

use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrl;

class ShortenedUrlViewModel
{
    /** @var string */
    private $shortUrl;

    /** @var string */
    private $sourceUrl;

    private function __construct($shortUrl, $sourceUrl)
    {
        $this->shortUrl = $shortUrl;
        $this->sourceUrl = $sourceUrl;
    }

    public static function fromShortenedUrl(ShortenedUrl $shortenedUrl)
    {
        return new self((string) $shortenedUrl->getShortUrl(), (string) $shortenedUrl->getSourceUrl());
    }

    public function getShortUrl(): string
    {
        return $this->shortUrl;
    }

    public function getSourceUrl(): string
    {
        return $this->sourceUrl;
    }
}

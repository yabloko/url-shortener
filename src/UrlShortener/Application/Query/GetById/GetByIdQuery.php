<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Application\Query\GetById;

class GetByIdQuery
{
    /** @var string */
    public $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}

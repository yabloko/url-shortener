<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Application\Query\GetById;

use Yabloko\UrlShortener\Application\Query\ViewModel\ShortenedUrlViewModel;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlReadRepositoryInterface;

class GetByIdHandler
{
    /** @var ShortenedUrlReadRepositoryInterface */
    private $repository;

    public function __construct(ShortenedUrlReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(GetByIdQuery $query): ?ShortenedUrlViewModel
    {
        $id = ShortenedUrlId::fromSignature($query->id);

        $shortenedUrl = array_reduce($this->repository->getAll(), function (ShortenedUrl $carry = null, ShortenedUrl $item) use ($id) {
            return $id->equals($item->getId()) ? $item : $carry;
        }, null);

        if($shortenedUrl instanceof ShortenedUrl) {
            return ShortenedUrlViewModel::fromShortenedUrl($shortenedUrl);
        }

        return null;
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

use Yabloko\UrlShortener\Domain\SequenceGenerator\SequenceGeneratorInterface;

final class ShortenedUrl
{
    /** @var ShortenedUrlId */
    private $id;

    /** @var ShortUrl */
    private $shortUrl;

    /** @var SourceUrl */
    private $sourceUrl;

    private function __construct(ShortenedUrlId $id, ShortUrl $shortUrl, SourceUrl $sourceUrl)
    {
        $this->id = $id;
        $this->shortUrl = $shortUrl;
        $this->sourceUrl = $sourceUrl;
    }

    public static function fromValues(ShortenedUrlId $id, ShortUrl $shortUrl, SourceUrl $sourceUrl): self
    {
        return new self($id, $shortUrl, $sourceUrl);
    }

    public static function createWithSequenceGenerator(ShortenedUrlId $id, SourceUrl $sourceUrl, SequenceGeneratorInterface $sequenceGenerator): self
    {
        $nextSequenceValue = $sequenceGenerator->next();
        $shortUrlValue = self::baseConvert($nextSequenceValue);
        $shortUrl = ShortUrl::fromValue($shortUrlValue);

        return new self($id, $shortUrl, $sourceUrl);
    }

    private static function baseConvert(int $nextSequenceValue): string
    {
        $res = base_convert($nextSequenceValue, 10, 26);
        $res = strtr($res,'0123456789','qrstuvxwyz');
        return $res;
    }

    public function getId(): ShortenedUrlId
    {
        return $this->id;
    }

    public function getShortUrl(): ShortUrl
    {
        return $this->shortUrl;
    }

    public function getSourceUrl(): SourceUrl
    {
        return $this->sourceUrl;
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

use Assert\Assert;

final class ShortUrl
{
    private const SHORT_URL_MAX_LENGTH = 8;

    /** @var string */
    private $shortUrl;

    private function __construct(string $shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }

    public static function fromValue(string $value): self
    {
        $pattern = sprintf('/^[a-z]{0,%d}$/', self::SHORT_URL_MAX_LENGTH);
        Assert::that($value)->regex($pattern, 'Shortened URL must contain only lowercase letters and be 8 characters max.');

        return new self($value);
    }

    public function __toString()
    {
        return $this->shortUrl;
    }

    public function equals(ShortUrl $other): bool
    {
        return $this->shortUrl === $other->shortUrl;
    }
}

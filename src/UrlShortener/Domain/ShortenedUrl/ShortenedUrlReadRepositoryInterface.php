<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

interface ShortenedUrlReadRepositoryInterface
{
    public function find(ShortenedUrlId $id): ?ShortenedUrl;

    public function findBySourceUrl(SourceUrl $sourceUrl): ?ShortenedUrl;

    public function findByShortUrl(ShortUrl $shortUrl): ?ShortenedUrl;

    public function getAll(): array;
}

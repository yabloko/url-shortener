<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

use Assert\Assert;

final class SourceUrl
{
    /** @var string */
    private $sourceUrl;

    private function __construct(string $sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;
    }

    public static function fromValue(string $sourceUrl): self
    {
        Assert::that($sourceUrl)->url();

        return new self($sourceUrl);
    }

    public function __toString()
    {
        return $this->sourceUrl;
    }

    public function equals(SourceUrl $other): bool
    {
        return $this->sourceUrl === $other->sourceUrl;
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

use Assert\Assert;
use Rhumsaa\Uuid\Uuid;

final class ShortenedUrlId
{
    /** @var string */
    private $signature;

    private function __construct(string $signature)
    {
        $this->signature = $signature;
    }

    public static function fromSignature(string $signature): self
    {
        Assert::that($signature)->uuid();

        return new self($signature);
    }

    public static function create()
    {
        return new self((string) Uuid::uuid4());
    }

    public function __toString()
    {
        return $this->signature;
    }

    public function equals(ShortenedUrlId $other): bool
    {
        return $this->signature === $other->signature;
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

interface ShortenedUrlWriteRepositoryInterface
{
    public function save(ShortenedUrl $shortenedUrl);
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\ShortenedUrl;

interface ShortenedUrlRepositoryInterface extends ShortenedUrlReadRepositoryInterface, ShortenedUrlWriteRepositoryInterface
{
}

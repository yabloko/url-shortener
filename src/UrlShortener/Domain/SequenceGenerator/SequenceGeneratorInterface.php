<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Domain\SequenceGenerator;

interface SequenceGeneratorInterface
{
    public function next(): int;
}

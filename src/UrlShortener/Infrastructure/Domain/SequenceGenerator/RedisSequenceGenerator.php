<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Infrastructure\Domain\SequenceGenerator;

use Yabloko\UrlShortener\Domain\SequenceGenerator\SequenceGeneratorInterface;

class RedisSequenceGenerator implements SequenceGeneratorInterface
{
    private const SEQUENCE_KEY_NAME = 'url-shortener-sequence-key';

    /** @var \Redis */
    private $redis;

    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    public function next(): int
    {
        return $this->redis->incr(self::SEQUENCE_KEY_NAME);
    }
}

<?php

declare(strict_types=1);

namespace Yabloko\UrlShortener\Infrastructure\Domain\ShortenedUrl;

use Assert\Assert;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlId;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortenedUrlRepositoryInterface;
use Yabloko\UrlShortener\Domain\ShortenedUrl\ShortUrl;
use Yabloko\UrlShortener\Domain\ShortenedUrl\SourceUrl;

class RedisShortenedUrlRepository implements ShortenedUrlRepositoryInterface
{
    private const SHORT_URL_KEY_NAME = 'shortUrl';
    private const SOURCE_URL_KEY_NAME = 'sourceUrl';
    private const ID_KEY_NAME = 'id';

    /** @var \Redis */
    private $redis;

    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    public function find(ShortenedUrlId $id): ?ShortenedUrl
    {
        if (($json = $this->redis->get((string) $id)) !== false) {
            return $this->shortenedUrlFromArrayData($this->jsonDecodeOrThrow($json));
        }

        return null;
    }

    public function getAll(): array
    {
        return $this->fetchAll();
    }

    public function findBySourceUrl(SourceUrl $sourceUrl): ?ShortenedUrl
    {
        $filteredUrls = array_filter($this->fetchAll(), function (ShortenedUrl $shortenedUrl) use ($sourceUrl) {
            return $shortenedUrl->getSourceUrl()->equals($sourceUrl);
        });

        if(count($filteredUrls) > 1) {
            throw new \LogicException('Unique constraint violation - more than one record has same source url value.');
        }

        return array_shift($filteredUrls);
    }

    public function findByShortUrl(ShortUrl $shortUrl): ?ShortenedUrl
    {
        $filteredUrls = array_filter($this->fetchAll(), function (ShortenedUrl $shortenedUrl) use ($shortUrl) {
            return $shortenedUrl->getShortUrl()->equals($shortUrl);
        });

        if(count($filteredUrls) > 1) {
            throw new \LogicException('Unique constraint violation - more than one record has same short url value.');
        }

        return array_shift($filteredUrls);
    }

    public function save(ShortenedUrl $shortenedUrl)
    {
        if($this->redis->exists((string) $shortenedUrl->getId())) {
            throw new \RuntimeException(sprintf('Shortened URL with id %s already exists.', $shortenedUrl->getId()));
        }

        $this->redis->append(
            (string) $shortenedUrl->getId(),
            json_encode([
                self::ID_KEY_NAME => (string) $shortenedUrl->getId(),
                self::SHORT_URL_KEY_NAME => (string) $shortenedUrl->getShortUrl(),
                self::SOURCE_URL_KEY_NAME => (string) $shortenedUrl->getSourceUrl(),
            ])
        );
    }

    private function jsonDecodeOrThrow(string $json): array
    {
        $arrayData = json_decode($json, true);
        if(json_last_error()) {
            throw new \RuntimeException(sprintf('Failed fetching ShortenedUrl from storage: %s', json_last_error_msg()));
        }

        return $arrayData;
    }

    private function shortenedUrlFromArrayData(array $arrayData): ShortenedUrl
    {
        Assert::that($arrayData)
            ->keyExists(self::SHORT_URL_KEY_NAME)
            ->keyExists(self::SOURCE_URL_KEY_NAME);

        return ShortenedUrl::fromValues(
            ShortenedUrlId::fromSignature($arrayData[self::ID_KEY_NAME]),
            ShortUrl::fromValue($arrayData[self::SHORT_URL_KEY_NAME]),
            SourceUrl::fromValue($arrayData[self::SOURCE_URL_KEY_NAME])
        );
    }

    private function fetchAll(): array
    {
        $keys = $this->redis->keys('*');

        return array_map(function (string $key) {
            return $this->shortenedUrlFromArrayData($this->jsonDecodeOrThrow($this->redis->get($key)));
        }, $keys);
    }
}
